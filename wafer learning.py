import sys, os
import copy
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import PIL.Image as Image
import math
import cv2

from torchvision import transforms

from drnet import ResNet, resnet18, resnet34
from densenet import densenet121
from attentionnet import attention56
from inception import inceptionv4

sys.path.append(r'\\cvpmfsip03\MHDMA\Cobra\Main') # ..\Cobra

from utils.pubfuncs import file2df, df2file, list2str, find
from wafer_learning.ds_reshape import reshape_ds_param, unreshape_ds_param, get_df_numeric_cols
from wafer_learning.idw import idw_wafers
from mhdap.wlr_db_conn import db_conn
from wafer_learning.wl_preprocess import WLScaler
from sklearn.base import BaseEstimator,TransformerMixin

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.data.sampler import SubsetRandomSampler
import torchvision
import time

from torch.optim import lr_scheduler
from torchvision.datasets import ImageFolder
from torch.utils.data import Dataset, DataLoader

GLB_COL_KEYS = ('diey', 'diex', 'wafer', 'fablot')
class WLIDWImputer(BaseEstimator, TransformerMixin):
    def __init__(self, idw_p=4, col_keys=GLB_COL_KEYS):
        self.p=idw_p
        self.col_keys = list(col_keys)
    def fit(self,X, y=None):
        return self # nothing else to do
    def transform(self, X, y=None):
        assert isinstance(X, pd.DataFrame), 'X is not a Dataframe object!'
        df = copy.deepcopy(X)
        df = df.dropna(axis=1, how='all')
        df = df[list(dict.fromkeys(self.col_keys + get_df_numeric_cols(df)))]
        print(df.columns)
        df_idw = idw_wafers(df_wafers=df, p=self.p)
        print(df_idw.columns)
        df_idw = df_idw.dropna(axis=1, how='all')
        print(df_idw.columns)
        return df_idw


def data_prepare(fname):
    # delete blank wafers
    df1 = file2df(os.path.join(work_path, fname))
    # df2 = df1.drop_duplicates(subset=['fablot', 'wafer'], keep='first')
    # df = pd.DataFrame(columns=['fablot', 'wafer', 'diex', 'diey', 'bbk_wlldcc_viso_wod_d'])
    # # 'bbk_slcprogrand_d'
    # for i in df2.index:
    #     fablot, wafer = df2['fablot'].loc[i], df2['wafer'].loc[i]
    #     df3 = df1[['fablot', 'wafer', 'diex', 'diey', 'bbk_wlldcc_viso_wod_d']][(df1['fablot'] == fablot)
    #                                                                             & (df1['wafer'] == wafer)]
    #     df3 = df3.drop_duplicates(subset=['fablot', 'wafer', 'diex', 'diey'], keep='first')
    #     if sum(df3['bbk_wlldcc_viso_wod_d'].isnull()) < (len(df3) * 0.2):
    #         df = pd.concat([df, df3], axis=0, join='inner')

    # idw and clip data
    #add idw pre_process
    # imputer = WLIDWImputer(idw_p=4)
    # df_ds_idw = imputer.transform(df1)
    # df_ds_idw['bbk_wlldcc_viso_wod_d'] = df_ds_idw['bbk_wlldcc_viso_wod_d'].clip(lower=0, upper=255)
    # df2file(df_ds_idw, fname.split('.')[0] + '_idw', outpath=work_path, ftype='csv')

    # data_amplify1 = []
    # # # data_amplify2 = []
    # for i in df1.index:
    #     x = data_amplification(df1['bbk_wlldcc_viso_wod_d'].loc[i])
    #     # y = data_amplification(df1['bbk_slcprogrand_d'].loc[i])
    #     data_amplify1.append(x)
    # #     # data_amplify2.append(y)
    # df1['bbk_wlldcc_viso_wod_d'] = data_amplify1
    # # # df1['bbk_slcprogrand_d'] = data_amplify2
    # df2file(df1, '_three', outpath=work_path, ftype='csv')

def data_amplification(x):
    # if x <= 20:
    #     return x * 10
    # else:
    #     x = x + 200
    #     if x >= 255:
    #         return 25
    #     else:
    #         return x
    if x <= 2:
        return 0
    else:
        return 5

def wafer_data2tensor(fname):
    fig_path_0 = r'C:\Users\1000268745\fengpu test\bics5_1t_x3_wafermap_cluster10_2\0'
    fig_path_1 = r'C:\Users\1000268745\fengpu test\bics5_1t_x3_wafermap_cluster10_2\1'
    fig_path_2 = r'C:\Users\1000268745\fengpu test\bics5_1t_x3_wafermap_cluster10_2\2'
    fig_path_3 = r'C:\Users\1000268745\fengpu test\bics5_1t_x3_wafermap_cluster10_2\3'
    fig_path_4 = r'C:\Users\1000268745\fengpu test\bics5_1t_x3_wafermap_cluster10_2\4'
    fig_path_5 = r'C:\Users\1000268745\fengpu test\bics5_1t_x3_wafermap_cluster10_2\5'
    fig_path_6 = r'C:\Users\1000268745\fengpu test\bics5_1t_x3_wafermap_cluster10_2\6'
    fig_path_7 = r'C:\Users\1000268745\fengpu test\bics5_1t_x3_wafermap_cluster10_2\7'
    fig_path_8 = r'C:\Users\1000268745\fengpu test\bics5_1t_x3_wafermap_cluster10_2\8'
    fig_path_9 = r'C:\Users\1000268745\fengpu test\bics5_1t_x3_wafermap_cluster10_2\9'

    df1 = file2df(os.path.join(work_path, fname))
    df2 = df1.drop_duplicates(subset=['fablot', 'wafer'], keep='first')

    # dataframe to array
    for i in df2.index:
        id = str(df2['fablot'].loc[i]) + '_' + str(df2['wafer'].loc[i])
        label = str(df2['cluster_pred'].loc[i])
        fablot, wafer = df2['fablot'].loc[i], df2['wafer'].loc[i]
        df3 = df1[['diex', 'diey', 'bbk_wlldcc_viso_wod_d']][(df1['fablot'] == fablot) & (df1['wafer'] == wafer)]
        df4 = df3.pivot(index='diey', columns='diex', values='bbk_wlldcc_viso_wod_d')
        ary = np.array(df4)
        ary0 = np.nan_to_num(ary)
        ary0.astype(int)
        image0 = Image.fromarray(ary0).convert('RGB')
        if label == '0':
            fig_name = os.path.join(fig_path_0, id + '.png')
            image0.save(fig_name)
            image0.close()
        elif label == '1':
            fig_name = os.path.join(fig_path_1, id + '.png')
            image0.save(fig_name)
            image0.close()
        elif label == '2':
            fig_name = os.path.join(fig_path_2, id + '.png')
            image0.save(fig_name)
            image0.close()
        elif label == '3':
            fig_name = os.path.join(fig_path_3, id + '.png')
            image0.save(fig_name)
            image0.close()
        elif label == '4':
            fig_name = os.path.join(fig_path_4, id + '.png')
            image0.save(fig_name)
            image0.close()
        elif label == '5':
            fig_name = os.path.join(fig_path_5, id + '.png')
            image0.save(fig_name)
            image0.close()
        elif label == '6':
            fig_name = os.path.join(fig_path_6, id + '.png')
            image0.save(fig_name)
            image0.close()
        elif label == '7':
            fig_name = os.path.join(fig_path_7, id + '.png')
            image0.save(fig_name)
            image0.close()
        elif label == '8':
            fig_name = os.path.join(fig_path_8, id + '.png')
            image0.save(fig_name)
            image0.close()
        elif label == '9':
            fig_name = os.path.join(fig_path_9, id + '.png')
            image0.save(fig_name)
            image0.close()

def clustering_one_param(fname1,fname2):
    from sklearn.cluster import KMeans, AgglomerativeClustering, DBSCAN, MeanShift, estimate_bandwidth
    from sklearn.cluster import SpectralClustering

    df_train1 = file2df(os.path.join(work_path, fname1))
    df_train2 = file2df(os.path.join(work_path, fname2))
    df_train = pd.merge(left=df_train1, right=df_train2, on=['fablot', 'wafer','diex','diey','bbk_wlldcc_viso_wod_d'],
                        how='outer')

    df_train = WLScaler(col_keys=['fablot', 'wafer', 'diex', 'diey']).transform(df_train)

    df_train = reshape_ds_param(df_train)
    param = r'bbk_wlldcc_viso_wod_d'
    df_train = df_train.query(list2str(['parameter', '==', '\'', param, '\'']))
    df2file(df_train, fname1.split('.')[0] + '_reshape', outpath=work_path, ftype='csv')

    x_train_cols = list(df_train.columns)
    x_train_cols.remove('fablot')
    x_train_cols.remove('wafer')
    x_train_cols.remove('parameter')
    X = df_train[x_train_cols]

    def cluster_out(df_train, labels, algo_name, colname='cluster_pred'):
        df_train.insert(loc=0, column=colname, value=labels)
        df_train_unreshaped = unreshape_ds_param(df_train, index=['fablot', 'wafer', colname])

        df2file(df_train, fname=algo_name + r'_pred_10.csv', outpath=work_path, ftype='csv')
        df2file(df_train_unreshaped, fname=algo_name + r'_pred_unshaped_10.csv', outpath=work_path, ftype='csv')

    agc_en = True
    if agc_en:
        print('AffinityPropagation Clustering...')
        n_clusters = 10
        clustering = AgglomerativeClustering(linkage='ward', n_clusters=n_clusters)
        y_pred = clustering.fit_predict(X)
        cluster_out(df_train, labels=y_pred, algo_name='agglomerative', colname='cluster_pred')

def clustering_check(fname1,fname2):
    #把聚类结果和人工分类进行比对
    from sklearn.metrics import roc_auc_score

    df1 = file2df(os.path.join(work_path, fname1))
    df2 = file2df(os.path.join(work_path, fname2))
    df3 = df2[['fablot', 'wafer', 'diex', 'diey', 'cluster_pred']]
    df_merge = pd.merge(left=df3, right=df1, on=['fablot', 'wafer', 'diex', 'diey'], how='left')
    # print(df_merge)
    df2file(df_merge, 'cluster_check', outpath=work_path, ftype='csv')


def label_merge(fname1,fname2):
    df1 = file2df(os.path.join(work_path, fname1))
    df2 = file2df(os.path.join(work_path, fname2))
    df_merge = pd.merge(left=df1, right=df2, on=['fablot', 'wafer'], how='left')
    df2file(df_merge, 'merge', outpath=work_path, ftype='csv')

def extra_fablot_wafer(fname):
    df1 = file2df(os.path.join(work_path, fname))
    df2 = df1.drop_duplicates(subset=['fablot', 'wafer'], keep='first')
    df2file(df2[['fablot','wafer']], fname='1', outpath=work_path, ftype='csv')

def visualization_wafers(fname):
    fig_path_0 = r'C:\Users\1000268745\fengpu test\merge_two_wafermap\0'
    fig_path_1 = r'C:\Users\1000268745\fengpu test\merge_two_wafermap\1'
    fig_path_2 = r'C:\Users\1000268745\fengpu test\merge_two_wafermap\2'
    fig_path_3 = r'C:\Users\1000268745\fengpu test\merge_two_wafermap\3'
    fig_path_4 = r'C:\Users\1000268745\fengpu test\merge_two_wafermap\4'
    fig_path_5 = r'C:\Users\1000268745\fengpu test\merge_two_wafermap\5'
    fig_path_6 = r'C:\Users\1000268745\fengpu test\merge_two_wafermap\6'
    fig_path_7 = r'C:\Users\1000268745\fengpu test\merge_two_wafermap\7'
    fig_path_8 = r'C:\Users\1000268745\fengpu test\merge_two_wafermap\8'
    fig_path_9 = r'C:\Users\1000268745\fengpu test\merge_two_wafermap\9'

    df1 = file2df(os.path.join(work_path, fname))
    df2 = df1.drop_duplicates(subset=['fablot', 'wafer'], keep='first')

    for i in df2.index:
        id = str(df2['fablot'].loc[i]) + '_' + str(df2['wafer'].loc[i])
        label = str(df2['cluster_pred'].loc[i])
        fablot, wafer = df2['fablot'].loc[i], df2['wafer'].loc[i]
        df3 = df1[['diex', 'diey', 'bbk_wlldcc_viso_wod_d']][(df1['fablot'] == fablot) & (df1['wafer'] == wafer)]
        df4 = df3.pivot(index='diey', columns='diex', values='bbk_wlldcc_viso_wod_d')
        fig = plt.figure(num=id, figsize=(4, 4))
        ax = sns.heatmap(df4, cmap='gray', square=True, cbar=False)
        if label == '0':
            fig_name = os.path.join(fig_path_0, id)
            plt.savefig(fig_name)
            plt.close()
        elif label == '1':
            fig_name = os.path.join(fig_path_1, id)
            plt.savefig(fig_name)
            plt.close()
        elif label == '2':
            fig_name = os.path.join(fig_path_2, id)
            plt.savefig(fig_name)
            plt.close()
        elif label == '3':
            fig_name = os.path.join(fig_path_3, id)
            plt.savefig(fig_name)
            plt.close()
        elif label == '4':
            fig_name = os.path.join(fig_path_4, id)
            plt.savefig(fig_name)
            plt.close()
        elif label == '5':
            fig_name = os.path.join(fig_path_5, id)
            plt.savefig(fig_name)
            plt.close()
        elif label == '6':
            fig_name = os.path.join(fig_path_6, id)
            plt.savefig(fig_name)
            plt.close()
        elif label == '7':
            fig_name = os.path.join(fig_path_7, id)
            plt.savefig(fig_name)
            plt.close()
        elif label == '8':
            fig_name = os.path.join(fig_path_8, id)
            plt.savefig(fig_name)
            plt.close()
        elif label == '9':
            fig_name = os.path.join(fig_path_9, id)
            plt.savefig(fig_name)
            plt.close()

#ml part
# 数据加载的方式，一种是每一种分类分别放在一个文件夹，用imagefoder方便加载，文件名是lable；
# 还有一种就是有一个id和label的文件，按照这个来读取，需要重新写dataset


def transform_data(img_path):
    normalize = transforms.Normalize(mean=[.5, .5, .5], std=[.5, .5, .5])
    pre_transform = transforms.Compose([transforms.Resize(size=(32, 32)),
                                        transforms.ToTensor(),
                                        normalize])
    train_dataset = ImageFolder(os.path.join(img_path, 'train'), transform=pre_transform)
    train_loader = DataLoader(train_dataset, batch_size=10, shuffle=False, num_workers=0, drop_last=True)
    valid_dataset = ImageFolder(os.path.join(img_path, 'test'), transform=pre_transform)
    valid_loader = DataLoader(valid_dataset, batch_size=10, shuffle=False, num_workers=0, drop_last=True)
    train_image_name = []
    valid_image_name = []
    for address in train_dataset.imgs:
        train_image_name.append(address[0].split('\\')[7].split('.')[0])
    for i in valid_dataset.imgs:
        valid_image_name.append(i[0].split('\\')[7].split('.')[0])

    return train_loader, valid_loader, train_image_name, valid_image_name


def trainNet(epoch):
    print('Epoch {}'.format(epoch))
    # 加载数据集上边的方法解释了获取训练数据
    start_time = time.time()
    train_loss = 0
    model.train()

    for step, (x_batch, y_batch) in enumerate(train_loader):
        # forward:前向传播
        outputs = model(x_batch)
        # print(outputs.size(), x_batch.size())
        # y_batch = y_batch.to(torch.float32)
        loss = loss_func(outputs, y_batch)
        train_loss += loss.item()
        #backward:后向传播
        optimizer.zero_grad()  # 将所有的梯度置零，原因是防止每次backward的时候梯度会累加
        loss.backward()  # 根据反向传播更新所有的参数
        optimizer.step()
        if (step + 1) % 10 == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, (step + 1) * 10, len(train_loader.dataset), 100. * (step + 1) / len(train_loader), loss))
    # exp_lr_scheduler.step()     #调整学习率类似cv grid
    print("Training loss={}, took {:.2f}s".format(train_loss/(len(train_loader)), time.time() - start_time))
    # 所有的Epoch结束，也就是训练结束，计算花费的时间


def evaluate(data_loader):
    model.eval()
    loss = 0
    correct = 0

    for data, target in data_loader:

        output = model(data)

        # target = target.to(torch.float32)

        loss += F.cross_entropy(output, target)

        pred = output.data.argmax(1, keepdim=True)
        # pred = output.data
        # pred = pred > 0.5
        correct += pred.eq(target.data.view_as(pred)).cpu().sum()


    loss /= len(data_loader)

    print('\nAverage loss: {:.4f}, Accuracy: {}/{} ({:.3f}%)\n'.format(
        loss, correct, len(data_loader.dataset),
        100. * correct / len(data_loader.dataset)))


def prediciton(data_loader):
    model.eval()
    test_pred = torch.LongTensor()
    label_true = torch.LongTensor()

    for data, target in data_loader:
        output = model(data)
        pred = output.cpu().data.max(1, keepdim=True)[1]
        # pred = output.data
        # pred = pred > 0.5
        test_pred = torch.cat((test_pred, pred), dim=0)
        label_true = torch.cat((label_true, target), dim=0)

    return test_pred, label_true


class CNN(nn.Module):
    def __init__(self):
        super(CNN, self).__init__()

        self.features = nn.Sequential(
            nn.Conv2d(3, 32, kernel_size=(3, 3), stride=(1, 1), padding=(0, 0)),
            nn.BatchNorm2d(32),
            nn.ReLU(inplace=True),
            # nn.Conv2d(32, 32, kernel_size=(3, 3), stride=(1, 1), padding=(0, 0)),
            # nn.BatchNorm2d(32),
            # nn.ReLU(inplace=True),
            # nn.MaxPool2d(kernel_size=2, stride=2)
            # nn.Conv2d(32, 64, kernel_size=(3, 3), stride=(1, 1), padding=(0, 0)),
            # nn.BatchNorm2d(64),
            # nn.ReLU(inplace=True),
            # nn.Conv2d(64, 64, kernel_size=(3, 3), stride=(1, 1), padding=(0, 0)),
            # nn.BatchNorm2d(64),
            # nn.ReLU(inplace=True),
            # nn.MaxPool2d(kernel_size=2, stride=2)


        )

        self.classifier = nn.Sequential(
            nn.Dropout(p=0.5),
            nn.Linear(32 * 14 * 14, 512),
            # nn.BatchNorm1d(512),
            nn.ReLU(inplace=True),
            # nn.Dropout(p=0.5),
            nn.Linear(512, 120),
            # nn.BatchNorm1d(120),
            nn.ReLU(inplace=True),
            # nn.Dropout(p=0.5),
            nn.Linear(120, 7))

    def forward(self, x):
        x = self.features(x)
        # plt.imshow(x[0][0].detach().numpy())
        # plt.show()
        x = x.view(x.size(0), -1)
        x = self.classifier(x)
        # x = F.sigmoid(x)
        # x = x.view(x.size(0))

        return x



if __name__ == "__main__":
    work_path = r'C:\Users\1000268745\fengpu test'

    fname1 = r'merge_agglomerative_pred_unshaped_10.csv'
    fname2 = r'202103_b5x3_1t_bbk_visocc_cut_idw.csv'
    fname3 = r'b5_1t_x3_merge_amplification.csv'
    fname4 = r'_three.csv'


    img_1 = r'C:\Users\1000268745\fengpu test\b5x3_wafermap_l'
    img_2 = r'C:\Users\1000268745\fengpu test\bics5_1t_x3_wafermap_cluster10_5'
    img_3 = r'C:\Users\1000268745\fengpu test\merge_two_wafermap_3'


    # data_prepare(fname1)
    # wafer_data2tensor(fname4)
    # label_merge(fname1=fname4,fname2=fname2)
    # visualization_wafers(fname=fname4)
    # clustering_one_param(fname1=fname1, fname2=fname4)
    # clustering_check(fname3, fname4)

    # cnn 部分
    # train_loader, test_loader, train_image_name, test_image_name = transform_data(img_path=img_3)
    #
    # # model = CNN()
    # model = CNN()
    # loss_func = torch.nn.CrossEntropyLoss()  # 交叉熵损失函数
    # # loss_func = torch.nn.BCELoss()
    # # 优化器
    # optimizer = optim.Adam(model.parameters(), lr=0.00001)  # Adam 优化算法是随机梯度下降算法的扩展式
    # # exp_lr_scheduler = lr_scheduler.StepLR(optimizer, step_size=2, gamma=0.1)  # 调整学习率
    # for epoch in range(50):
    #     trainNet(epoch)
    #     evaluate(train_loader)
    #     evaluate(test_loader)
    # # # # #wafer learning输出形式
    # test_pred, label_true = prediciton(test_loader)
    # print(test_pred.numpy())
    # print(label_true.numpy())
    # out_df = pd.DataFrame(np.c_[np.array(test_image_name),
    #                       label_true.numpy(),
    #                       test_pred.numpy()],
    #                       columns=['ImageId', 'Label', 'prediction'])
    # df = out_df
    # print(df)
    # df2file(df, 'result', outpath=img_1, ftype='csv')

