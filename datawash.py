import pandas as pd
import os, re
import pandas as pd
import numpy as np

path = r'\\10.70.30.247\share\Chengxue\PACON'
pat_file = re.compile(r'(\w{9})_\d+_\d_\d_SME1_rawdata_wafer.csv')

def nomization(df):
    f1_1 = df.iloc[:, 4:]
    f1_2 = df.iloc[:, :4]
    f1_1 = f1_1.apply(lambda x: (x - np.min(x)) / (np.max(x) - np.min(x)) * 256)
    f1_3 = pd.concat([f1_2, f1_1], axis=1)
    return f1_3

df = pd.DataFrame(columns=['fablot', 'wafer', 'diex', 'diey'])
for file in os.listdir(path):
    if pat_file.search(file):
        print(file)
        df2 = pd.read_csv(path + '/' + file)
        df3 = df2.loc[:, ['fablot','wafer','diex','diey','bbk_wlrcsgs_d','bbk_visocc_wodd_d','bbk_slcprog_d','tprog_wl029']][df2['passfail']==1]
        df1 = nomization(df3)
        df = pd.concat([df, df1], axis=0, join='outer')
print(df)
#df = df.drop(['PF'], axis=1)
df2 = pd.read_csv(path + '/' + 'fail_label.csv')
df_new = pd.merge(df, df2, how='left', on=['fablot', 'wafer', 'diex', 'diey'])
print(df_new)
df_new.to_csv(path + '/' + 'rawdata_label_new.csv', index=False)
