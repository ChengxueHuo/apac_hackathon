import pandas as pd
import numpy as np

path = r'\\10.70.30.247\share\Chengxue\PACON'
#df = pd.read_csv(path + '/' + 'EP0388095_5_5_5_SME1_rawdata_wafer.csv')
'''
value_list = []
for parameter in ['sblk3_tprog_wl084_st0_p1', 'sblk3_tprog_wl085_st0_p0']:
    value_parameter_list = []
    for x in range(5, 51):
        value_temp_list = []
        for y in range(5, 29):
            if df[(df['diex'] == x) & (df['diey'] == y)].empty:
                value_temp_list.append(0)
            else:
                value_temp_list.append(df[(df['diex'] == x) & (df['diey'] == y)][parameter].tolist()[0])
        value_parameter_list.append(value_temp_list)
    value_list.append(value_parameter_list)
array_value = np.array(value_list)
print(array_value, array_value.shape)
'''
path1= r'C:\Users\1000259091\Downloads'
df = pd.read_csv(path1 + '/' + 'rawdata_idw_new.csv')
df_label = pd.read_csv(path1 + '/' + 'fail_dppm_label.csv')
fablot_wafer = df.loc[:, ['fablot', 'wafer']].drop_duplicates().values.tolist()
value_total = []
label_total = []
for i in range(len(fablot_wafer)):
    value_list = []
    label_total.append(df_label[(df_label['fablot'] == fablot_wafer[i][0]) & (df_label['wafer'] == fablot_wafer[i][1])]['label'].tolist()[0])
    df_wafer = df[(df['fablot'] == fablot_wafer[i][0]) & (df['wafer'] == fablot_wafer[i][1])]
    df_columns = df_wafer.columns.tolist()
    for j in range(4, len(df_columns)):
        value_parameter_list = []
        for x in range(5, 51):
            value_temp_list = []
            for y in range(5, 29):
                if df[(df['diex'] == x) & (df['diey'] == y)].empty:
                    value_temp_list.append(0)
                else:
                    value_temp_list.append(df[(df['diex'] == x) & (df['diey'] == y)][df_columns[j]].tolist()[0])
            value_parameter_list.append(value_temp_list)
        value_list.append(value_parameter_list)
    value_total.append(value_list)
array_value = np.array(value_total)
array_label = np.array(label_total)
print(array_value, array_value.shape)
print(array_label, array_label.shape)
np.save(path1 + '/' + 'train_x_4channel.npy', array_value)
np.save(path1 + '/' + 'train_y_4channel.npy', array_label)


