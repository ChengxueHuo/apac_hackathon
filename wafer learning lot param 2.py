import sys, os
import copy
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns


from torchvision import transforms
#
# from drnet import ResNet, resnet18, resnet34
# from densenet import densenet121
# from attentionnet import attention56
# from inception import inceptionv4

sys.path.append(r'\\cvpmfsip03\MHDMA\Cobra\Main') # ..\Cobra

from utils.pubfuncs import file2df, df2file, list2str, find
from wafer_learning.ds_reshape import reshape_ds_param, unreshape_ds_param, get_df_numeric_cols
from wafer_learning.idw import idw_wafers
from mhdap.wlr_db_conn import db_conn
from wafer_learning.wl_preprocess import WLScaler
from sklearn.base import BaseEstimator,TransformerMixin

from torch.utils.data import TensorDataset
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.data.sampler import SubsetRandomSampler
import torchvision
import time

from torch.optim import lr_scheduler
from torchvision.datasets import ImageFolder
from torch.utils.data import Dataset, DataLoader

GLB_COL_KEYS = ('diey', 'diex', 'wafer', 'fablot')



def visualization_wafers(fname1,fname2):
    fig_path_1 = r'C:\Users\1000268745\wafer-dppm\dataset3\train\1'
    fig_path_2 = r'C:\Users\1000268745\wafer-dppm\dataset3\train\2'
    fig_path_3 = r'C:\Users\1000268745\wafer-dppm\dataset3\train\3'
    fig_path_4 = r'C:\Users\1000268745\wafer-dppm\dataset3\train\4'

    df_mrq = file2df(os.path.join(work_path, fname1))
    df_all_wafer = df_mrq.drop_duplicates(subset=['fablot', 'wafer'], keep='first')
    df_sme = file2df(os.path.join(work_path, fname2))

    col = df_sme.columns.values
    feature_list = list(set(col) - set(GLB_COL_KEYS))
    for i in df_all_wafer.index:
        id = str(df_all_wafer['fablot'].loc[i]) + '_' + str(df_all_wafer['wafer'].loc[i])
        label = str(df_all_wafer['label'].loc[i])
        fablot, wafer = df_all_wafer['fablot'].loc[i], df_all_wafer['wafer'].loc[i]
        df3 = df_sme[feature_list][(df_sme['fablot'] == fablot) & (df_sme['wafer'] == wafer)]
        if df3.empty is False:
            fig = plt.figure(num=id, figsize=(5, 5))
            ax = sns.heatmap(df3, cmap='gray', square=True, cbar=False, yticklabels=False, xticklabels=False)
            if label == '1':
                fig_name = os.path.join(fig_path_1, id)
                plt.savefig(fig_name)
                plt.close()
            elif label == '2':
                fig_name = os.path.join(fig_path_2, id)
                plt.savefig(fig_name)
                plt.close()
            elif label == '3':
                fig_name = os.path.join(fig_path_3, id)
                plt.savefig(fig_name)
                plt.close()
            elif label == '4':
                fig_name = os.path.join(fig_path_4, id)
                plt.savefig(fig_name)
                plt.close()

def transform_data(img_path):
    normalize = transforms.Normalize(mean=[.5, .5, .5], std=[.5, .5, .5])
    pre_transform = transforms.Compose([transforms.Resize(size=(32, 32)),
                                        transforms.ToTensor(),
                                        normalize])
    train_dataset = ImageFolder(os.path.join(img_path, 'train'), transform=pre_transform)
    train_loader = DataLoader(train_dataset, batch_size=10, shuffle=False, num_workers=0, drop_last=True)
    valid_dataset = ImageFolder(os.path.join(img_path, 'test'), transform=pre_transform)
    valid_loader = DataLoader(valid_dataset, batch_size=10, shuffle=False, num_workers=0, drop_last=True)
    train_image_name = []
    valid_image_name = []
    for address in train_dataset.imgs:
        train_image_name.append(address[0].split('\\')[7].split('.')[0])
    for i in valid_dataset.imgs:
        valid_image_name.append(i[0].split('\\')[7].split('.')[0])

    return train_loader, valid_loader, train_image_name, valid_image_name


def trainNet(epoch):
    print('Epoch {}'.format(epoch))
    # 加载数据集上边的方法解释了获取训练数据
    start_time = time.time()
    train_loss = 0
    model.train()

    for step, (x_batch, y_batch) in enumerate(train_loader):
        # forward:前向传播
        outputs = model(x_batch)
        # print(outputs.size(), x_batch.size())
        # y_batch = y_batch.to(torch.float32)
        loss = loss_func(outputs, y_batch)
        train_loss += loss.item()
        #backward:后向传播
        optimizer.zero_grad()  # 将所有的梯度置零，原因是防止每次backward的时候梯度会累加
        loss.backward()  # 根据反向传播更新所有的参数
        optimizer.step()
        if (step + 1) % 10 == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, (step + 1) * 10, len(train_loader.dataset), 100. * (step + 1) / len(train_loader), loss))
    # exp_lr_scheduler.step()     #调整学习率类似cv grid
    print("Training loss={}, took {:.2f}s".format(train_loss/(len(train_loader)), time.time() - start_time))
    # 所有的Epoch结束，也就是训练结束，计算花费的时间


def evaluate(data_loader):
    model.eval()
    loss = 0
    correct = 0

    for data, target in data_loader:

        output = model(data)

        # target = target.to(torch.float32)

        loss += F.cross_entropy(output, target)

        pred = output.data.argmax(1, keepdim=True)
        # pred = output.data
        # pred = pred > 0.5
        correct += pred.eq(target.data.view_as(pred)).cpu().sum()


    loss /= len(data_loader)

    print('\nAverage loss: {:.4f}, Accuracy: {}/{} ({:.3f}%)\n'.format(
        loss, correct, len(data_loader.dataset),
        100. * correct / len(data_loader.dataset)))


def prediciton(data_loader):
    model.eval()
    test_pred = torch.LongTensor()
    label_true = torch.LongTensor()

    for data, target in data_loader:
        output = model(data)
        pred = output.cpu().data.max(1, keepdim=True)[1]
        # pred = output.data
        # pred = pred > 0.5
        test_pred = torch.cat((test_pred, pred), dim=0)
        label_true = torch.cat((label_true, target), dim=0)

    return test_pred, label_true


class CNN(nn.Module):
    def __init__(self):
        super(CNN, self).__init__()

        self.features = nn.Sequential(
            nn.Conv2d(3, 32, kernel_size=(3, 3), stride=(1, 1), padding=(0, 0)),
            nn.BatchNorm2d(32),
            nn.ReLU(inplace=True),
            nn.Conv2d(32, 32, kernel_size=(3, 3), stride=(1, 1), padding=(0, 0)),
            nn.BatchNorm2d(32),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=2, stride=2),
            nn.Conv2d(32, 64, kernel_size=(3, 3), stride=(1, 1), padding=(0, 0)),
            nn.BatchNorm2d(64),
            nn.ReLU(inplace=True),
            nn.Conv2d(64, 64, kernel_size=(3, 3), stride=(1, 1), padding=(0, 0)),
            nn.BatchNorm2d(64),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=2, stride=2)
        )

        self.classifier = nn.Sequential(
            nn.Dropout(p=0.5),
            nn.Linear(64 * 5 * 5, 400),
            # nn.BatchNorm1d(512),
            nn.ReLU(inplace=True),
            # nn.Dropout(p=0.5),
            nn.Linear(400, 120),
            # nn.BatchNorm1d(120),
            nn.ReLU(inplace=True),
            # nn.Dropout(p=0.5),
            nn.Linear(120, 3))

    def forward(self, x):
        x = self.features(x)
        # plt.imshow(x[0][0].detach().numpy())
        # plt.show()
        x = x.view(x.size(0), -1)
        x = self.classifier(x)
        # x = F.sigmoid(x)
        # x = x.view(x.size(0))

        return x

if __name__ == "__main__":
    work_path = r'C:\Users\1000268745\wafer-dppm'
    fname1 = r'fail_dppm_label.csv'
    fname2 = r'rawdata_idw_scale.csv'
    fname3 = r'rawdata_idw.csv'

    img_1 = r'C:\Users\1000268745\wafer-dppm\dataset'

    visualization_wafers(fname1=fname1,fname2=fname3)
    # cnn 部分
    # train_loader, test_loader, train_image_name, test_image_name = transform_data(img_path=img_1)
    #
    # model = CNN()
    # loss_func = torch.nn.CrossEntropyLoss()  # 交叉熵损失函数
    # # # loss_func = torch.nn.BCELoss()
    # # # 优化器
    # optimizer = optim.Adam(model.parameters(), lr=0.0001)  # Adam 优化算法是随机梯度下降算法的扩展式
    # # # exp_lr_scheduler = lr_scheduler.StepLR(optimizer, step_size=2, gamma=0.1)  # 调整学习率
    # for epoch in range(20):
    #     trainNet(epoch)
    #     evaluate(train_loader)
    #     evaluate(test_loader)
    # # # # #wafer learning输出形式
    # test_pred, label_true = prediciton(test_loader)
