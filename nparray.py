import sys, os
import copy
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


# from torchvision import transforms
#
# from drnet import ResNet, resnet18, resnet34
# from densenet import densenet121
# from attentionnet import attention56
# from inception import inceptionv4

sys.path.append(r'\\cvpmfsip03\MHDMA\Cobra\Main') # ..\Cobra

from utils.pubfuncs import file2df, df2file, list2str, find
from wafer_learning.ds_reshape import reshape_ds_param, unreshape_ds_param, get_df_numeric_cols
from wafer_learning.idw import idw_wafers
from mhdap.wlr_db_conn import db_conn
from wafer_learning.wl_preprocess import WLScaler
from sklearn.base import BaseEstimator,TransformerMixin
'''
x_new = []
y_new = []
x = np.load('train_x.npy')
y = np.load('train_y.npy')
print(x, x.shape)
print(y, y.shape)
for i in range(y.shape[0]):
    if y[i] == 3:
        j = 15
        while j > 0:
            x_new.append(x[i])
            y_new.append(y[i])
            j = j-1
    elif y[i] == 2:
        j = 15
        while j > 0:
            x_new.append(x[i])
            y_new.append(y[i])
            j = j-1
    else:
        x_new.append(x[i])
        y_new.append(y[i])
x_array = np.array(x_new)
y_array = np.array(y_new)
print(x_array, x_array.shape)
print(y_array, y_array.shape)
np.save('train_x_balance.npy', x_array)
np.save('train_y_balance.npy', y_array)


x_new = []
y_new = []
x = np.load('train_x_balance.npy')
y = np.load('train_y_balance.npy')
print(x.shape, y.shape)
for i in range(x.shape[0]):
    x_new_temp = []
    for j in (0, 4, 16, 47):
        x_new_temp.append(x[i][j])
    x_new.append(x_new_temp)
    y_new.append(y[i])
x_array = np.array(x_new) * 255
y_array = np.array(y_new)
print(x_array, x_array.shape)
print(y_array, y_array.shape)
np.save('train_x_4channel.npy', x_array)
np.save('train_y_4channel.npy', y_array)
'''

GLB_COL_KEYS = ('diey', 'diex', 'wafer', 'fablot')
class WLIDWImputer(BaseEstimator, TransformerMixin):
    def __init__(self, idw_p=4, col_keys=GLB_COL_KEYS):
        self.p=idw_p
        self.col_keys = list(col_keys)
    def fit(self,X, y=None):
        return self # nothing else to do
    def transform(self, X, y=None):
        assert isinstance(X, pd.DataFrame), 'X is not a Dataframe object!'
        df = copy.deepcopy(X)
        df = df.dropna(axis=1, how='all')
        df = df[list(dict.fromkeys(self.col_keys + get_df_numeric_cols(df)))]
        print(df.columns)
        df_idw = idw_wafers(df_wafers=df, p=self.p)
        print(df_idw.columns)
        df_idw = df_idw.dropna(axis=1, how='all')
        print(df_idw.columns)
        return df_idw
path = r'\\10.70.30.247\share\Chengxue\PACON'
df = pd.read_csv(path + '/' + 'rawdata_label_new.csv')

df_idw = WLIDWImputer().transform(df)
df_idw.to_csv(path + '/' + 'rawdata_idw_new.csv')