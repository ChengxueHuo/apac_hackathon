import sys, os
import copy
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


# from torchvision import transforms
#
# from drnet import ResNet, resnet18, resnet34
# from densenet import densenet121
# from attentionnet import attention56
# from inception import inceptionv4

sys.path.append(r'\\cvpmfsip03\MHDMA\Cobra\Main') # ..\Cobra

from utils.pubfuncs import file2df, df2file, list2str, find
from wafer_learning.ds_reshape import reshape_ds_param, unreshape_ds_param, get_df_numeric_cols
from wafer_learning.idw import idw_wafers
from mhdap.wlr_db_conn import db_conn
from wafer_learning.wl_preprocess import WLScaler
from sklearn.base import BaseEstimator,TransformerMixin

from torch.utils.data import TensorDataset
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.data.sampler import SubsetRandomSampler
import torchvision
import time

from torch.optim import lr_scheduler
from torchvision.datasets import ImageFolder
from torch.utils.data import Dataset, DataLoader

GLB_COL_KEYS = ('diey', 'diex', 'wafer', 'fablot')
class WLIDWImputer(BaseEstimator, TransformerMixin):
    def __init__(self, idw_p=4, col_keys=GLB_COL_KEYS):
        self.p=idw_p
        self.col_keys = list(col_keys)
    def fit(self,X, y=None):
        return self # nothing else to do
    def transform(self, X, y=None):
        assert isinstance(X, pd.DataFrame), 'X is not a Dataframe object!'
        df = copy.deepcopy(X)
        df = df.dropna(axis=1, how='all')
        df = df[list(dict.fromkeys(self.col_keys + get_df_numeric_cols(df)))]
        print(df.columns)
        df_idw = idw_wafers(df_wafers=df, p=self.p)
        print(df_idw.columns)
        df_idw = df_idw.dropna(axis=1, how='all')
        print(df_idw.columns)
        return df_idw

def data_pre(fname):
    # idw and clip data
    #add idw pre_process
    df = file2df(os.path.join(work_path, fname))
    imputer = WLIDWImputer(idw_p=4)
    df_ds_idw = imputer.transform(df)
    # df_ds_idw['bbk_wlldcc_viso_wod_d'] = df_ds_idw['bbk_wlldcc_viso_wod_d'].clip(lower=0, upper=255)
    df2file(df_ds_idw, fname.split('.')[0] + '_idw', outpath=work_path, ftype='csv')
    # col = df.columns.values
    # feature_list = list(set(col) - set(GLB_COL_KEYS))
    # for i in feature_list:
    #     df[i] = df[i].apply(data_amplification)
    # df2file(df, fname.split('.')[0] + '_binarization', outpath=work_path, ftype='csv')

def data_amplification(x):
    if x <= 2:
        return 0
    else:
        return 5

def visualization_wafers(fname1,fname2):
    fig_path_1 = r'C:Users\1000268745\wafer-dppm\dataset\train\1'
    fig_path_2 = r'C:Users\1000268745\wafer-dppm\dataset\train\2'
    fig_path_3 = r'C:Users\1000268745\wafer-dppm\dataset\train\3'
    fig_path_4 = r'C:Users\1000268745\wafer-dppm\dataset\train\4'

    df_mrq = file2df(os.path.join(work_path, fname1))
    df_all_wafer = df_mrq.drop_duplicates(subset=['fablot', 'wafer'], keep='first')
    df_sme = file2df(os.path.join(work_path, fname2))

    col = df_sme.columns.values
    feature_list = list(set(col) - set(GLB_COL_KEYS))
    for i in df_all_wafer.index:
        id = str(df_all_wafer['fablot'].loc[i]) + '_' + str(df_all_wafer['wafer'].loc[i])
        label = str(df_all_wafer['label'].loc[i])
        fablot, wafer = df_all_wafer['fablot'].loc[i], df_all_wafer['wafer'].loc[i]
        df3 = df_sme[feature_list][(df_sme['fablot'] == fablot) & (df_sme['wafer'] == wafer)]
        fig = plt.figure(num=id, figsize=(5, 5))
        ax = sns.heatmap(df3, cmap='gray', square=True, cbar=False, yticklabels=False, xticklabels=False)
        if label == '1':
            fig_name = os.path.join(fig_path_1, id)
            fig = ax.get_figure()
            fig.savefig(fig_name)
            plt.close()
        elif label == '2':
            fig_name = os.path.join(fig_path_2, id)
            fig = ax.get_figure()
            fig.savefig(fig_name)
            plt.close()
        elif label == '3':
            fig_name = os.path.join(fig_path_3, id)
            fig = ax.get_figure()
            fig.savefig(fig_name)
            plt.close()
        elif label == '4':
            fig_name = os.path.join(fig_path_4, id)
            fig = ax.get_figure()
            fig.savefig(fig_name)
            plt.close()

def scale_df(fname):
    df = file2df(os.path.join(work_path, fname5))
    col = df.columns.values
    feature_list = list(set(col) - set(GLB_COL_KEYS))
    for c in feature_list:
        d = df[c]
        MAX = d.max()
        MIN = d.min()
        df[c] = ((d - MIN)/(MAX - MIN))
        # print(df[c])
    df2file(df, fname='1', outpath=work_path, ftype='csv')

def label_merge(fname1,fname2):
    df1 = file2df(os.path.join(work_path, fname1))
    df2 = file2df(os.path.join(work_path, fname2))
    df_merge = pd.merge(left=df1, right=df2, on=['fablot', 'wafer'], how='left')
    df2file(df_merge, 'merge', outpath=work_path, ftype='csv')

def extra_fablot_wafer(fname):
    df1 = file2df(os.path.join(work_path, fname))
    df2 = df1.drop_duplicates(subset=['fablot', 'wafer'], keep='first')
    df2file(df2[['fablot','wafer']], fname='1', outpath=work_path, ftype='csv')

def tensor_generate(fname1,fname2):

    df = file2df(os.path.join(work_path, fname1))
    df_label = file2df(os.path.join(work_path, fname2))
    fablot_wafer = df.loc[:, ['fablot', 'wafer']].drop_duplicates().values.tolist()
    value_total = []
    label_total = []
    for i in range(len(fablot_wafer)):
        value_list = []
        label_total.append(
            df_label[(df_label['fablot'] == fablot_wafer[i][0]) & (df_label['wafer'] == fablot_wafer[i][1])][
                'label'].tolist()[0])
        df_wafer = df[(df['fablot'] == fablot_wafer[i][0]) & (df['wafer'] == fablot_wafer[i][1])]
        df_columns = df_wafer.columns.tolist()
        for j in range(4, len(df_columns)):
            value_parameter_list = []
            for x in range(5, 51):
                value_temp_list = []
                for y in range(5, 29):
                    if df[(df['diex'] == x) & (df['diey'] == y)].empty:
                        value_temp_list.append(0)
                    else:
                        value_temp_list.append(df[(df['diex'] == x) & (df['diey'] == y)][df_columns[j]].tolist()[0])
                value_parameter_list.append(value_temp_list)
            value_list.append(value_parameter_list)
        value_total.append(value_list)
    array_value = np.array(value_total)
    array_label = np.array(label_total)
    np.save('train_x.npy',array_value)
    np.save('train_y.npy',array_label)
    print(array_value, array_value.shape)
    print(array_label, array_label.shape)
    return array_value,array_label

class CNN(nn.Module):
    def __init__(self):
        super(CNN, self).__init__()

        self.features = nn.Sequential(
            nn.Conv2d(in_channels=69, out_channels=1, kernel_size=(5, 5), stride=(1, 1), padding=(0, 0)),
            nn.BatchNorm2d(1),
            nn.ReLU(inplace=True),
            # nn.Conv2d(32, 32, kernel_size=(3, 3), stride=(1, 1), padding=(0, 0)),
            # nn.BatchNorm2d(32),
            # nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=2, stride=2))
            # nn.Conv2d(32, 64, kernel_size=(3, 3), stride=(1, 1), padding=(0, 0)),
            # nn.BatchNorm2d(64),
            # nn.ReLU(inplace=True),
            # nn.Conv2d(64, 64, kernel_size=(3, 3), stride=(1, 1), padding=(0, 0)),
            # nn.BatchNorm2d(64),
            # nn.ReLU(inplace=True),
            # nn.MaxPool2d(kernel_size=2, stride=2)

        self.classifier = nn.Sequential(
            nn.Dropout(p=0.5),
            nn.Linear(210, 512),
            # nn.BatchNorm1d(512),
            nn.ReLU(inplace=True),
            # nn.Dropout(p=0.5),
            nn.Linear(512, 120),
            # nn.BatchNorm1d(120),
            nn.ReLU(inplace=True),
            # nn.Dropout(p=0.5),
            nn.Linear(120, 3))

    def forward(self, x):
        x = self.features(x)
        # plt.imshow(x[0][0].detach().numpy())
        # plt.show()
        x = x.view(x.size(0), -1)
        x = self.classifier(x)
        # x = F.sigmoid(x)
        # x = x.view(x.size(0))

        return x

def transform_data(ary1,ary2):
    a = torch.tensor(ary1)
    b = torch.tensor(ary2)
    # print(a,b)
    train_ids = TensorDataset(a.float(),b.float())
    # for x_train, y_label in train_ids:
    #     print(x_train,y_label)
    train_loader = DataLoader(dataset=train_ids, batch_size=1, shuffle=True)
    return train_loader

def trainNet(epoch):
    print('Epoch {}'.format(epoch))
    # 加载数据集上边的方法解释了获取训练数据
    start_time = time.time()
    train_loss = 0
    model.train()
    for step, (x_batch, y_batch) in enumerate(train_loader):
        print(step,x_batch, x_batch.shape)
        print(y_batch, y_batch.shape)
        # forward:前向传播
        outputs = model(x_batch)
        # print(outputs)
        # print(y_batch)
        # print(outputs.size(), x_batch.size())
        y_batch = y_batch.type(torch.LongTensor)
        y_batch = y_batch.unsqueeze(1)
        loss = loss_func(outputs, y_batch)
        train_loss += loss.item()
        #backward:后向传播
        optimizer.zero_grad()  # 将所有的梯度置零，原因是防止每次backward的时候梯度会累加
        loss.backward()  # 根据反向传播更新所有的参数
        optimizer.step()
        if (step + 1) % 10 == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, (step + 1) * 10, len(train_loader.dataset), 100. * (step + 1) / len(train_loader), loss))
    # exp_lr_scheduler.step()     #调整学习率类似cv grid
    print("Training loss={}, took {:.2f}s".format(train_loss/(len(train_loader)), time.time() - start_time))
    # 所有的Epoch结束，也就是训练结束，计算花费的时间


def evaluate(data_loader):
    model.eval()
    loss = 0
    correct = 0

    for data, target in data_loader:

        output = model(data)

        # target = target.to(torch.float32)

        loss += F.cross_entropy(output, target)

        pred = output.data.argmax(1, keepdim=True)
        # pred = output.data
        # pred = pred > 0.5
        correct += pred.eq(target.data.view_as(pred)).cpu().sum()


    loss /= len(data_loader)

    print('\nAverage loss: {:.4f}, Accuracy: {}/{} ({:.3f}%)\n'.format(
        loss, correct, len(data_loader.dataset),
        100. * correct / len(data_loader.dataset)))

if __name__ == "__main__":
    work_path = r'C:\Users\1000268745\wafer-dppm'

    fname1 = r'bics5_512g_d3_2p.csv'
    fname2 = r'bics5_512g_d3_2p_idw.csv'
    fname3 = r'bics5_512g_d3_2p_idw_binarization.csv'
    fname4 = r'fail_dppm_label.csv'
    fname5 = r'rawdata_idw.csv'
    fname6 = r'rawdata_idw_scale.csv'
    # data_pre(fname=fname5)
    # array_value, array_label = tensor_generate()
    # np.save('x.npy',array_value)
    # np.save('y.npy',array_label)
    # transform_data(ary1=array_value,ary2=array_label)
    # train_dataset = array_value
    # train_loader = DataLoader(train_dataset, batch_size=10, shuffle=False, num_workers=0, drop_last=True)

    # tensor_generate(fname1=fname6,fname2=fname4)

    train_loader = transform_data(ary1=(np.load('train_x.npy')),ary2=(np.load('train_y.npy')))


    model = CNN()
    # loss_func = torch.nn.CrossEntropyLoss()  # 交叉熵损失函数
    loss_func = torch.nn.BCELoss()
    # 优化器
    optimizer = optim.Adam(model.parameters(), lr=0.00001)  # Adam 优化算法是随机梯度下降算法的扩展式
    # exp_lr_scheduler = lr_scheduler.StepLR(optimizer, step_size=2, gamma=0.1)  # 调整学习率
    for epoch in range(50):
        trainNet(epoch)
        evaluate(train_loader)
        # evaluate(test_loader)
    # # # #wafer learning输出形式
    # test_pred, label_true = prediciton(test_loader)
    # print(test_pred.numpy())
    # print(label_true.numpy())





